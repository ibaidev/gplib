
GPlib
=====

A python library for Gaussian Process Regression.

Setup GPlib
-----------

- Create and activate virtualenv (for python2) or
  venv (for python3)
    ```bash
    # for python3
    python3 -m venv .env
    # or for python2
    python2 -m virtualenv .env

    source .env/bin/activate
    ```

- Upgrade pip
    ```bash
    python -m pip install --upgrade pip
    ```

- Install GPlib package
    ```bash
    python -m pip install gplib
    ```

- Matplotlib requires to install a backend to work interactively
  (See https://matplotlib.org/faq/virtualenv_faq.html).
  The easiest solution is to install the Tk framework,
  which can be found as python-tk (or python3-tk) on
  certain Linux distributions.


Use GPlib
----------------------

- Import GPlib to use it in your python script.
    ```python
    import gplib
    ```

- Initialize the GP with the desired modules.
    ```python
    gp = gplib.GP(
        mean_function=gplib.mea.Fixed(),
        covariance_function=gplib.ker.SquaredExponential()
    )
    ```

- Plot the GP.
    ```python
    gplib.plot.gp_1d(gp, n_samples=10)
    ```

- Generate some random data.
    ```python
    import numpy as np
    data = {
        'X': np.arange(3, 8, 1.0)[:, None],
        'Y': np.random.uniform(0, 2, 5)[:, None]
    }
    ```

- Get the posterior GP given the data.
    ```python
    posterior_gp = gp.get_posterior(data)
    ```

- Finally plot the posterior GP.
    ```python
    gplib.plot.gp_1d(posterior_gp, data, n_samples=10)
    ```

- There are more examples in examples/ directory. Check them out!

Develop GPlib
-------------

-  Download the repository using git
    ```bash
    git clone https://gitlab.com/ibaidev/gplib.git
    ```

-  Update API documentation
    ```bash
    source ./.env/bin/activate
    pip install Sphinx
    cd docs/
    sphinx-apidoc -f -o ./ ../gplib
    ```
