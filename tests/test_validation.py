# -*- coding: utf-8 -*-
#
#    Copyright 2019 Ibai Roman
#
#    This file is part of GPlib.
#
#    GPlib is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GPlib is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GPlib. If not, see <http://www.gnu.org/licenses/>.

import unittest

import numpy as np

from gplib.data_manipulations.validation import Validation

class ValidationTest(unittest.TestCase):
    """ Inference test set """
    pass


def validation_test_generator(problem, n_reps, stratified,
                              shuffle, folds_n, fold_len):
    """
    Generate inference tests

    :param problem:
    :type problem:
    :param n_reps:
    :type n_reps:
    :param stratified:
    :type stratified:
    :param shuffle:
    :type shuffle:
    :param folds_n:
    :type folds_n:
    :param fold_len:
    :type fold_len:
    :return:
    :rtype:
    """

    if problem == 'classification':
        data_set = {
            'X': np.arange(0, 100, 0.1)[:, None],
            'Y': np.random.choice([b'a', b'b'], size=1000)[:, None]
        }
    elif problem == 'regression':
        data_set = {
            'X': np.arange(0, 100, 0.1)[:, None],
            'Y': np.random.uniform(0, 10, 1000)[:, None]
        }
    else:
        raise Exception("problem not found")

    def test_validation(self):
        """

        :param self:
        :type self:
        :return:
        :rtype:
        """

        folds = Validation.split_data(
            data_set,
            n_reps,
            stratified,
            shuffle,
            folds_n,
            fold_len
        )

        np.testing.assert_allclose(
            sum([len(_set['X']) for sets in folds for _set in sets if _set]),
            n_reps * folds_n * len(data_set['X'])
        )

        np.testing.assert_allclose(
            np.std([
                (float(len(train_set['X'])) /
                 len(test_set['X']) if test_set is not None else 0.0)
                for train_set, test_set in folds
            ]),
            0.0,
            atol=0.1
        )

    return test_validation


for problem in ['classification', 'regression']:
    for n_reps in [1, 2]:
        for stratified in [True, False]:
            for shuffle in [True, False]:
                for folds_n in [1, 3]:
                    for fold_len in [None, 0.0, 0.1]:
                        test_validation = \
                            validation_test_generator(
                                problem,
                                n_reps,
                                stratified,
                                shuffle,
                                folds_n,
                                fold_len
                            )
                        setattr(
                            ValidationTest,
                            "test_validation_{}_{}reps_{}_{}_{}folds_{}".format(
                                problem,
                                n_reps,
                                "stratified" if stratified else "",
                                "shuffle" if shuffle else "",
                                folds_n,
                                int(10 * fold_len) if fold_len is not None \
                                    else ""
                            ),
                            test_validation
                        )
