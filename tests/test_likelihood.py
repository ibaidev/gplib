# -*- coding: utf-8 -*-
#
#    Copyright 2019 Ibai Roman
#
#    This file is part of GPlib.
#
#    GPlib is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GPlib is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GPlib. If not, see <http://www.gnu.org/licenses/>.

import unittest
import copy

import numpy as np

import gplib


class LikelihoodTest(unittest.TestCase):
    """ Inference test set """
    pass


def likelihood_test_generator(kernel_class, noisy, ard):
    """ Generate inference tests """

    data = {
        'X': np.array([
            [0.1, 0.3, 0.4, 0.6, 0.9],
            [0.5, 0.2, 0.5, 0.5, 0.6]
        ]).transpose(),
        'Y': np.array([
            [0.5, 0.8, 0.8, 0.2, 0.5]
        ]).transpose()
    }

    gp = gplib.GP(
        mean_function=gplib.mea.Fixed(),
        covariance_function=kernel_class(ls=np.ones(2).tolist() if ard else 1.),
        noise=noisy
    )
    if noisy:
        gp.noise_covariance_function.kernel_function.set_param_values([1.0])

    def test_likelihood_function_grad(self):
        """ Test the gradient of the inference method """
        orig_params = gp.get_param_values(
            only_group=gplib.hp.Parameter.OPT_GROUP
        )
        lml, grad_lml = gp.get_log_likelihood(
            data, grad_needed=True
        )

        assert len(grad_lml) == len(orig_params),\
            "grad_lml and orig_params must have the same length"

        for i in range(len(grad_lml)):
            mod_params = copy.deepcopy(orig_params)
            mod_params[i] = mod_params[i] * np.exp(1e-05)
            gp.set_param_values(
                mod_params,
                only_group=gplib.hp.Parameter.OPT_GROUP
            )
            mod_lml = gp.get_log_likelihood(data)
            np.testing.assert_allclose(((mod_lml - lml) / 1e-05),
                                       grad_lml[i], rtol=1e-1)
            gp.set_param_values(
                orig_params,
                only_group=gplib.hp.Parameter.OPT_GROUP
            )

    return test_likelihood_function_grad


# TODO test periodic kernel
for kernel_class in [
            gplib.ker.Exponential,
            gplib.ker.GammaExponential15,
            gplib.ker.Matern32,
            gplib.ker.Matern52,
            gplib.ker.RationalQuadratic,
            gplib.ker.SquaredExponential
        ]:
    for noisy in [True, False]:
        for ard in [True, False]:
            test_likelihood_function_grad = likelihood_test_generator(
                kernel_class,
                noisy,
                ard
            )
            setattr(
                LikelihoodTest,
                'test_grad_{}_{}_{}'.format(
                    kernel_class.__name__,
                    "noisy" if noisy else "noiseless",
                    "ard" if ard else "noard"
                ),
                test_likelihood_function_grad
            )
