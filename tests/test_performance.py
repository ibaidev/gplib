# -*- coding: utf-8 -*-
#
#    Copyright 2019 Ibai Roman
#
#    This file is part of GPlib.
#
#    GPlib is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GPlib is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GPlib. If not, see <http://www.gnu.org/licenses/>.

import time
import unittest

import numpy as np

import gplib


class PerformanceTest(unittest.TestCase):
    """ Performance test set """

    def test_performance(self):
        """ Parameter Test """
        data = np.random.rand(2000, 2)
        data_new = np.random.rand(10, 2)
        data_full = np.vstack((data, data_new))

        gp = gplib.GP(
            mean_function=gplib.mea.Fixed(),
            covariance_function=gplib.ker.Sum([
                gplib.ker.Periodic(
                    gplib.ker.SquaredExponential(),
                ),
                gplib.ker.WhiteNoise()
            ]),
            noise=False,
            cache_size=10
        )

        noise_param = gp.get_kernel_function().kernels[1].hyperparameters[0]

        for i in range(5):
            rand_value1 = 1e-7 * np.random.uniform()
            rand_value2 = 1e-7 * np.random.uniform()
            noise_param.current_value = rand_value1
            PerformanceTest.check_data(gp, data)
            PerformanceTest.check_data(gp, data_full, compare_chol_time=True)

            noise_param.current_value = rand_value2
            PerformanceTest.check_data(gp, data_full)
            PerformanceTest.check_data(gp, data, compare_chol_time=True)

            noise_param.current_value = rand_value1
            PerformanceTest.check_data(gp, data, compare_chol_time=True)
            PerformanceTest.check_data(gp, data_full, compare_chol_time=True)

    @staticmethod
    def check_data(gp, data, compare_chol_time=False):
        """

        :param gp:
        :type gp:
        :param data:
        :type data:
        :param compare_chol_time:
        :type compare_chol_time:
        :return:
        :rtype:
        """
        start = time.time()
        cov_matrix = gp.covariance_function.marginalize_covariance(data)
        chol_l = gplib.MGD._chol(cov_matrix)
        end = time.time()
        chol_l_time = end - start

        start = time.time()
        mgd = gp.marginalize_gp(data)
        end = time.time()
        chol_l_get_time = end - start

        start = time.time()
        mgd2 = gp.marginalize_gp(data)
        end = time.time()
        chol_l_get_time2 = end - start

        if compare_chol_time:
            np.testing.assert_array_less(
                chol_l_get_time,
                chol_l_time
            )
        np.testing.assert_array_less(
            chol_l_get_time2,
            chol_l_time
        )
        np.testing.assert_array_almost_equal(
            np.dot(chol_l, chol_l.T),
            cov_matrix
        )
        np.testing.assert_array_almost_equal(
            np.dot(mgd.l_matrix, mgd.l_matrix.T),
            cov_matrix
        )
        np.testing.assert_array_almost_equal(
            np.dot(mgd2.l_matrix, mgd2.l_matrix.T),
            cov_matrix,
        )
