# -*- coding: utf-8 -*-
#
#    Copyright 2019 Ibai Roman
#
#    This file is part of GPlib.
#
#    GPlib is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GPlib is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GPlib. If not, see <http://www.gnu.org/licenses/>.

import unittest
import numpy as np

import gplib.parameters as hp


class ParameterTest(unittest.TestCase):
    """ Parameter test set """

    def test_param1(self):
        """ Parameter Test """
        param = hp.Parameter(
            'test_param',
            transformation=hp.LogParameterTransformation,
            default_value=11
        )
        np.random.seed(1)
        res = param.get_grid(trans=False)
        np.random.seed(1)
        log_res = param.get_grid(trans=True)
        np.testing.assert_allclose(len(res), 1)
        np.testing.assert_allclose(len(res), np.unique(res, axis=0).shape[0])
        np.testing.assert_allclose(len(res[0]), 1)
        np.testing.assert_allclose(np.log(res), log_res)

    def test_param2(self):
        """ Parameter Test """
        param = hp.Parameter(
            'test_param',
            transformation=hp.LogParameterTransformation,
            default_value=[11]
        )
        np.random.seed(1)
        res = param.get_grid(trans=False)
        np.random.seed(1)
        log_res = param.get_grid(trans=True)
        np.testing.assert_allclose(len(res), 1)
        np.testing.assert_allclose(len(res), np.unique(res, axis=0).shape[0])
        np.testing.assert_allclose(len(res[0]), 1)
        np.testing.assert_allclose(np.log(res), log_res)

    def test_param3(self):
        """ Parameter Test """
        param = hp.Parameter(
            'test_param',
            transformation=hp.LogParameterTransformation,
            default_value=[11, 12]
        )
        np.random.seed(1)
        res = param.get_grid(trans=False)
        np.random.seed(1)
        log_res = param.get_grid(trans=True)
        np.testing.assert_allclose(len(res), 1)
        np.testing.assert_allclose(len(res), np.unique(res, axis=0).shape[0])
        np.testing.assert_allclose(len(res[0]), 2)
        np.testing.assert_allclose(np.log(res), log_res)

    def test_param4(self):
        """ Parameter Test """
        param = hp.Parameter(
            'test_param',
            transformation=hp.LogParameterTransformation,
            default_value=[[11]]
        )
        np.random.seed(1)
        res = param.get_grid(trans=False)
        np.random.seed(1)
        log_res = param.get_grid(trans=True)
        np.testing.assert_allclose(len(res), 1)
        np.testing.assert_allclose(len(res), np.unique(res, axis=0).shape[0])
        np.testing.assert_allclose(len(res[0]), 1)
        np.testing.assert_allclose(np.log(res), log_res)

    def test_param5(self):
        """ Parameter Test """
        param = hp.Parameter(
            'test_param',
            transformation=hp.LogParameterTransformation,
            default_value=[[11], [12]]
        )
        np.random.seed(1)
        res = param.get_grid(trans=False)
        np.random.seed(1)
        log_res = param.get_grid(trans=True)
        np.testing.assert_allclose(len(res), 1)
        np.testing.assert_allclose(len(res), np.unique(res, axis=0).shape[0])
        np.testing.assert_allclose(len(res[0]), 2)
        np.testing.assert_allclose(np.log(res), log_res)

    def test_param6(self):
        """ Parameter Test """
        param = hp.Parameter(
            'test_param',
            transformation=hp.LogParameterTransformation,
            default_value=[[11, 12]]
        )
        np.random.seed(1)
        res = param.get_grid(trans=False)
        np.random.seed(1)
        log_res = param.get_grid(trans=True)
        np.testing.assert_allclose(len(res), 2)
        np.testing.assert_allclose(len(res), np.unique(res, axis=0).shape[0])
        np.testing.assert_allclose(len(res[0]), 1)
        np.testing.assert_allclose(np.log(res), log_res)

    def test_param7(self):
        """ Parameter Test """
        param = hp.Parameter(
            'test_param',
            transformation=hp.LogParameterTransformation,
            default_value=[[11, 12], [11], [13, 14, 15]]
        )
        np.random.seed(1)
        res = param.get_grid(trans=False)
        np.random.seed(1)
        log_res = param.get_grid(trans=True)
        np.testing.assert_allclose(len(res), 6)
        np.testing.assert_allclose(len(res), np.unique(res, axis=0).shape[0])
        np.testing.assert_allclose(len(res[0]), 3)
        np.testing.assert_allclose(np.log(res), log_res)

    def test_param8(self):
        """ Parameter Test """
        param = hp.Parameter(
            'test_param',
            transformation=hp.LogParameterTransformation,
            default_value=[[i for i in range(10)] for _ in range(4)]
        )
        np.random.seed(1)
        res = param.get_grid(trans=False, limit=1e3)
        np.random.seed(1)
        log_res = param.get_grid(trans=True, limit=1e3)
        np.testing.assert_allclose(len(res), 1e3)
        np.testing.assert_allclose(len(res), np.unique(res, axis=0).shape[0])
        np.testing.assert_allclose(len(res[0]), 4)
        np.testing.assert_allclose(np.log(res), log_res)

    def test_param9(self):
        """ Parameter Test """
        param = hp.Parameter(
            'test_param',
            transformation=hp.LogParameterTransformation,
            default_value=[[i for i in range(10)] for _ in range(10)]
        )
        np.random.seed(1)
        res = param.get_grid(trans=False, limit=1e3)
        np.random.seed(1)
        log_res = param.get_grid(trans=True, limit=1e3)
        np.testing.assert_allclose(len(res), 1e3)
        np.testing.assert_allclose(len(res[0]), 10)
        np.testing.assert_allclose(np.log(res), log_res)

    def test_with_param1(self):
        """ Parameter Test """
        param = hp.WithParameters("Test", [
            hp.Parameter(
                'test_param1',
                transformation=hp.LogParameterTransformation,
                default_value=[[11, 12]]
            ),
            hp.Parameter(
                'test_param2',
                transformation=hp.LogParameterTransformation,
                default_value=[[11], [13, 14, 15]]
            )
        ])
        np.random.seed(1)
        res = param.get_grid(trans=False)
        np.random.seed(1)
        log_res = param.get_grid(trans=True)
        np.testing.assert_allclose(len(res), 6)
        np.testing.assert_allclose(len(res[0]), 3)
        np.testing.assert_allclose(np.log(res), log_res)

    def test_with_param2(self):
        """ Parameter Test """
        param = hp.WithParameters("Test2", [])
        res = param.get_grid(trans=False)
        np.testing.assert_allclose(len(res), 0)