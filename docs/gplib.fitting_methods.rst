gplib.fitting\_methods package
==============================

Submodules
----------

gplib.fitting\_methods.fitting\_method module
---------------------------------------------

.. automodule:: gplib.fitting_methods.fitting_method
   :members:
   :undoc-members:
   :show-inheritance:

gplib.fitting\_methods.grid\_search module
------------------------------------------

.. automodule:: gplib.fitting_methods.grid_search
   :members:
   :undoc-members:
   :show-inheritance:

gplib.fitting\_methods.local\_search module
-------------------------------------------

.. automodule:: gplib.fitting_methods.local_search
   :members:
   :undoc-members:
   :show-inheritance:

gplib.fitting\_methods.multi\_start module
------------------------------------------

.. automodule:: gplib.fitting_methods.multi_start
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gplib.fitting_methods
   :members:
   :undoc-members:
   :show-inheritance:
