gplib.covariance\_functions package
===================================

Submodules
----------

gplib.covariance\_functions.covariance\_function module
-------------------------------------------------------

.. automodule:: gplib.covariance_functions.covariance_function
   :members:
   :undoc-members:
   :show-inheritance:

gplib.covariance\_functions.kernel\_based module
------------------------------------------------

.. automodule:: gplib.covariance_functions.kernel_based
   :members:
   :undoc-members:
   :show-inheritance:

gplib.covariance\_functions.posterior module
--------------------------------------------

.. automodule:: gplib.covariance_functions.posterior
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gplib.covariance_functions
   :members:
   :undoc-members:
   :show-inheritance:
