gplib.mean\_functions package
=============================

Submodules
----------

gplib.mean\_functions.constant module
-------------------------------------

.. automodule:: gplib.mean_functions.constant
   :members:
   :undoc-members:
   :show-inheritance:

gplib.mean\_functions.fixed module
----------------------------------

.. automodule:: gplib.mean_functions.fixed
   :members:
   :undoc-members:
   :show-inheritance:

gplib.mean\_functions.mean\_function module
-------------------------------------------

.. automodule:: gplib.mean_functions.mean_function
   :members:
   :undoc-members:
   :show-inheritance:

gplib.mean\_functions.posterior module
--------------------------------------

.. automodule:: gplib.mean_functions.posterior
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gplib.mean_functions
   :members:
   :undoc-members:
   :show-inheritance:
