gplib package
=============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   gplib.covariance_functions
   gplib.data_manipulations
   gplib.fitting_methods
   gplib.kernel_functions
   gplib.mean_functions
   gplib.metrics
   gplib.parameters

Submodules
----------

gplib.gaussian\_process module
------------------------------

.. automodule:: gplib.gaussian_process
   :members:
   :undoc-members:
   :show-inheritance:

gplib.multivariate\_gaussian module
-----------------------------------

.. automodule:: gplib.multivariate_gaussian
   :members:
   :undoc-members:
   :show-inheritance:

gplib.plot module
-----------------

.. automodule:: gplib.plot
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gplib
   :members:
   :undoc-members:
   :show-inheritance:
