gplib.kernel\_functions package
===============================

Submodules
----------

gplib.kernel\_functions.constant module
---------------------------------------

.. automodule:: gplib.kernel_functions.constant
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.dot\_product\_function module
-----------------------------------------------------

.. automodule:: gplib.kernel_functions.dot_product_function
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.exponential module
------------------------------------------

.. automodule:: gplib.kernel_functions.exponential
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.gamma\_exponential15 module
---------------------------------------------------

.. automodule:: gplib.kernel_functions.gamma_exponential15
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.kernel\_function module
-----------------------------------------------

.. automodule:: gplib.kernel_functions.kernel_function
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.linear module
-------------------------------------

.. automodule:: gplib.kernel_functions.linear
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.matern32 module
---------------------------------------

.. automodule:: gplib.kernel_functions.matern32
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.matern52 module
---------------------------------------

.. automodule:: gplib.kernel_functions.matern52
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.periodic module
---------------------------------------

.. automodule:: gplib.kernel_functions.periodic
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.product module
--------------------------------------

.. automodule:: gplib.kernel_functions.product
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.rational\_quadratic module
--------------------------------------------------

.. automodule:: gplib.kernel_functions.rational_quadratic
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.squared\_exponential module
---------------------------------------------------

.. automodule:: gplib.kernel_functions.squared_exponential
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.stationary\_function module
---------------------------------------------------

.. automodule:: gplib.kernel_functions.stationary_function
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.sum module
----------------------------------

.. automodule:: gplib.kernel_functions.sum
   :members:
   :undoc-members:
   :show-inheritance:

gplib.kernel\_functions.white\_noise module
-------------------------------------------

.. automodule:: gplib.kernel_functions.white_noise
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gplib.kernel_functions
   :members:
   :undoc-members:
   :show-inheritance:
