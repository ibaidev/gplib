gplib.metrics package
=====================

Submodules
----------

gplib.metrics.bic module
------------------------

.. automodule:: gplib.metrics.bic
   :members:
   :undoc-members:
   :show-inheritance:

gplib.metrics.lml module
------------------------

.. automodule:: gplib.metrics.lml
   :members:
   :undoc-members:
   :show-inheritance:

gplib.metrics.loocv module
--------------------------

.. automodule:: gplib.metrics.loocv
   :members:
   :undoc-members:
   :show-inheritance:

gplib.metrics.metric module
---------------------------

.. automodule:: gplib.metrics.metric
   :members:
   :undoc-members:
   :show-inheritance:

gplib.metrics.pearsonr module
-----------------------------

.. automodule:: gplib.metrics.pearsonr
   :members:
   :undoc-members:
   :show-inheritance:

gplib.metrics.predictive\_density module
----------------------------------------

.. automodule:: gplib.metrics.predictive_density
   :members:
   :undoc-members:
   :show-inheritance:

gplib.metrics.rmse module
-------------------------

.. automodule:: gplib.metrics.rmse
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gplib.metrics
   :members:
   :undoc-members:
   :show-inheritance:
