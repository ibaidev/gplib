# -*- coding: utf-8 -*-
#
#    Copyright 2019 Ibai Roman
#
#    This file is part of GPlib.
#
#    GPlib is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GPlib is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GPlib. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import gplib


def gp_regression_example(data):
    """
    regression example with GPs

    :param data:
    :type data:
    :return:
    :rtype:
    """

    validation = gplib.dm.Unfold(fold_len=0.2)
    train_set, test_set2 = validation.get_folds(data)[0]
    transformation = gplib.dm.DataTransformation(train_set)
    train_set = transformation.transform(train_set)
    test_set2 = transformation.transform(test_set2)
    train_set1, test_set1 = validation.get_folds(train_set)[0]
    train_set2 = test_set1

    gp = gplib.GP(
        mean_function=gplib.mea.Fixed(),
        covariance_function=gplib.ker.Periodic(
            gplib.ker.SquaredExponential(),
        )
    )

    metric = gplib.me.LML()

    fitting_method = gplib.fit.MultiStart(
        obj_fun=metric.fold_measure,
        max_fun_call=2500,
        nested_fit_method=gplib.fit.LocalSearch(
            obj_fun=metric.fold_measure,
            method="Powell",
            max_fun_call=1000
        )
    )

    validation = gplib.dm.Full()

    # We can chain posterior GPs
    log = fitting_method.fit(gp, validation.get_folds(
        train_set1
    ))
    print("{}".format(log))
    posterior_gp1 = gp.get_posterior(train_set1)

    posterior_lml = posterior_gp1.get_log_likelihood(
        test_set1
    )
    print("{}".format(posterior_lml))

    gplib.plot.gp_1d(
        posterior_gp1,
        train_set1,
        test_set1
    )

    log = fitting_method.fit(posterior_gp1, validation.get_folds(
        train_set2
    ))
    print("{}".format(log))
    posterior_gp2 = posterior_gp1.get_posterior(train_set2)

    posterior_lml = posterior_gp2.get_log_likelihood(
        test_set2
    )
    print("{}".format(posterior_lml))

    gplib.plot.gp_1d(
        posterior_gp2,
        train_set2,
        test_set2
    )


def gen_data(n=200):
    """

    :type n:
    :return:
    """

    gp = gplib.GP(
        mean_function=gplib.mea.Fixed(),
        covariance_function=gplib.ker.Sum([
            gplib.ker.Periodic(
                gplib.ker.SquaredExponential(),
            ),
            gplib.ker.WhiteNoise(ov2=0.2)
        ])
    )

    data = dict()
    data['X'] = np.arange(10., 20., 10./n)[:, None]
    data['Y'] = gp.sample(data['X'], n_samples=1)

    data['X'] *= np.power(10, np.random.uniform(-5, 5))
    data['Y'] *= np.power(10, np.random.uniform(-5, 5))

    return data


if __name__ == '__main__':
    gp_regression_example(gen_data())
