# -*- coding: utf-8 -*-
#
#    Copyright 2019 Ibai Roman
#
#    This file is part of GPlib.
#
#    GPlib is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GPlib is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GPlib. If not, see <http://www.gnu.org/licenses/>.

import gplib


def kernel_example():
    """

    :return:
    :rtype:
    """

    kernels = [
        gplib.ker.Constant(),
        gplib.ker.WhiteNoise()
    ]

    for kernel_class in [
                gplib.ker.Exponential,
                gplib.ker.GammaExponential15,
                gplib.ker.Matern32,
                gplib.ker.Matern52,
                gplib.ker.RationalQuadratic,
                gplib.ker.SquaredExponential,
                gplib.ker.Linear
            ]:
        kernels.append(kernel_class())

    kernels.append(gplib.ker.Sum([kernels[2], kernels[4]]))

    kernels.append(gplib.ker.Product([kernels[2], kernels[4]]))

    kernels.append(gplib.ker.Periodic(kernels[7]))

    for kernel in kernels:
        gplib.plot.kernel(kernel)


if __name__ == "__main__":
    kernel_example()
