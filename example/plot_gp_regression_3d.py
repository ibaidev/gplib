# -*- coding: utf-8 -*-
#
#    Copyright 2019 Ibai Roman
#
#    This file is part of GPlib.
#
#    GPlib is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GPlib is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GPlib. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import gplib


def gp_regression_example(data):
    """
    regression example with GPs

    :param data:
    :type data:
    :return:
    :rtype:
    """

    validation = gplib.dm.RandFold(fold_len=0.2, n_folds=1)
    train_set, test_set = validation.get_folds(data)[0]

    gp = gplib.GP(
        mean_function=gplib.mea.Fixed(),
        covariance_function=gplib.ker.Sum([
            gplib.ker.SquaredExponential(ls=np.ones(2).tolist()),
            gplib.ker.Linear()
        ])
    )

    metric = gplib.me.LML()

    fitting_method = gplib.fit.MultiStart(
        obj_fun=metric.fold_measure,
        max_fun_call=10000,
        nested_fit_method=gplib.fit.LocalSearch(
            obj_fun=metric.fold_measure,
            method="Powell",
            max_fun_call=2500
        )
    )

    validation = gplib.dm.Full()
    train_folds = validation.get_folds(train_set)

    log = fitting_method.fit(gp, train_folds)
    print("{}".format(log))

    posterior_gp = gp.get_posterior(train_set)

    posterior_lml = posterior_gp.get_log_likelihood(
        test_set
    )
    print("{}".format(posterior_lml))

    gplib.plot.gp_2d(
        posterior_gp,
        train_set,
        test_set
    )


def gen_data(n=200):
    """

    :param n:
    :type n:
    :return:
    :rtype:
    """

    gp = gplib.GP(
        mean_function=gplib.mea.Fixed(),
        covariance_function=gplib.ker.Sum([
            gplib.ker.SquaredExponential(ls=np.ones(2).tolist()),
            gplib.ker.Linear(),
            gplib.ker.WhiteNoise(ov2=0.2)
        ])
    )

    data = dict()
    data['X'] = np.hstack((
        np.random.uniform(0, 10, n)[:, None],
        np.random.uniform(0, 10, n)[:, None]
    ))
    data['Y'] = gp.sample(data['X'], n_samples=1)

    return data


if __name__ == '__main__':
    gp_regression_example(gen_data())
