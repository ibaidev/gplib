from .gaussian_process import GaussianProcess as GP
from .multivariate_gaussian import MultivariateGaussian as MGD
from . import covariance_functions as cov
from . import kernel_functions as ker
from . import mean_functions as mea
from . import fitting_methods as fit
from . import metrics as me
from . import data_manipulations as dm
from . import parameters as hp
from . import plot